package samnon.chips;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.MultiAutoCompleteTextView;
import android.widget.TextView;
import android.widget.Toast;

import com.tokenautocomplete.TokenCompleteTextView;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Random;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.multiAutoCompleteTextView)
    MultiAutoCompleteTextView autoCompleteTextView;
    @BindView(R.id.search)
    searchCompletionView searchCompletionView;

    @BindView(R.id.textView2)
    TextView textView;
    private HashSet<filterDataClass> currentFilters;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        currentFilters = new HashSet<>();
    }

    @Override
    protected void onResume() {
        super.onResume();

        dataAdapter adapter1 = new dataAdapter(getData());
        autoCompleteTextView.setAdapter(adapter1);
        autoCompleteTextView.setTokenizer(new MultiAutoCompleteTextView.CommaTokenizer());
        autoCompleteTextView.setThreshold(1);
        autoCompleteTextView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                filterDataClass filterDataClass = (samnon.chips.filterDataClass) parent.getAdapter().getItem(position);
                Toast.makeText(MainActivity.this, filterDataClass.getFilterString(), Toast.LENGTH_SHORT).show();
            }
        });
        searchCompletionView.setAdapter(adapter1);
        searchCompletionView.allowDuplicates(false);
        searchCompletionView.setTokenListener(new TokenCompleteTextView.TokenListener<filterDataClass>() {
            @Override
            public void onTokenAdded(filterDataClass token) {
                if (!currentFilters.contains(token)) {
                    currentFilters.add(token);
                    updateTextView();
                } else {
                    searchCompletionView.removeObject(token);
                    Toast.makeText(MainActivity.this, "Removed Duplicate", Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onTokenRemoved(filterDataClass token) {
               currentFilters.remove(token);
                updateTextView();
            }
        });
    }

    private void updateTextView() {

        // searching for ammount/ (amount, ammount)

        ArrayList<filterDataClass> ammountList = new ArrayList<>();
        ArrayList<filterDataClass> last4Digits = new ArrayList<>();
        ArrayList<filterDataClass> dateList = new ArrayList<>();
        ArrayList<filterDataClass> keywordList = new ArrayList<>();
        for (filterDataClass currentFilter : currentFilters) {
            String s = currentFilter.getType();
            if ("amount".equalsIgnoreCase(s)) {
                ammountList.add(currentFilter);

            } else if ("last4Digits".equalsIgnoreCase(s)) {
                last4Digits.add(currentFilter);

            } else if ("date".equalsIgnoreCase(s)) {
                dateList.add(currentFilter);

            } else if ("keyword".equalsIgnoreCase(s)) {
                keywordList.add(currentFilter);
            }
        }

        if (!currentFilters.isEmpty()) {
            textView.setText("Filtered By : " +  getString(ammountList," Amount ") +  getString(last4Digits,"Last 4 Digits ") + " " + getString(dateList,"Dates ") + " " + getString(keywordList, "Keywords "));
        } else {
            textView.setText("No Filters applied");
        }


    }

    private String getString(ArrayList<filterDataClass> list, String startingString) {
        StringBuilder sb = null;
        if (list.size() > 1) {
            sb = new StringBuilder(startingString + "( ");
            for (filterDataClass filterDataClass : list) {
                if(filterDataClass.getType().equalsIgnoreCase("amount"))
                {
                    sb.append(" $").append(filterDataClass.getFilterString());
                }else {
                    sb.append(" ").append(filterDataClass.getFilterString());
                }
            }
            sb.append(" )");
            sb.append("\n");
        } else if (!list.isEmpty()) {
            sb = new StringBuilder(startingString);
            if(list.get(0).getType().equalsIgnoreCase("amount"))
            {
                sb.append("$"+list.get(0).getFilterString());
            }
            else {
                sb.append(list.get(0).getFilterString());
            }
            sb.append("\n");
        }
        return sb != null ? sb.toString() : "";
    }


    private static final String[] COUNTRIES = new String[]{
            "Belgium", "France", "Italy", "Germany", "Spain"
    };

    private static final String[] names = new String[]{
            "Mothana", "Stephen", "Sam", "Darien", "Shalakha", "Claudia"
    };

    private static final String[] types = new String[]{
            "ATM", "CARD"
    };

    private ArrayList<filterDataClass> getData() {
        HashSet<DataClass> dataClasses = new HashSet<>();
        HashSet<filterDataClass> filterDataClasses = new HashSet<>();
        for (int i = 0; i < 50; i++) {
            DataClass dataClass = new DataClass(genDesc(), genAmount(i), getType(), getDate());
            dataClasses.add(dataClass);
        }

        for (DataClass dataClass : dataClasses) {
            filterDataClass desc = new filterDataClass(dataClass.getDesc(), 1, "desc");
            filterDataClass amount = new filterDataClass("$ " + dataClass.getAmount(), 1, "amount");
            filterDataClass type = new filterDataClass(dataClass.getType(), 1, "type");
            filterDataClass date = new filterDataClass(dataClass.getDate(), 1, "date");

            filterDataClasses.add(desc);
            filterDataClasses.add(amount);
            filterDataClasses.add(type);
            filterDataClasses.add(date);
        }
        return new ArrayList<>(filterDataClasses);
    }

    private String getDate() {
        return "12/12/12";
    }

    private String getType() {

        Random random = new Random();
        return types[random.nextInt(types.length)];
    }

    private float genAmount(int i) {
        Random random = new Random(i);
        int z = random.nextInt(2);
        if (z > 0) {
            return random.nextFloat() * 1000;
        } else {
            return random.nextFloat() * -100;
        }
    }

    private String genDesc() {
        Random randomn = new Random();

        int x = randomn.nextInt(10);
        int y = randomn.nextInt(COUNTRIES.length);
        int z = randomn.nextInt(names.length);

        if (x < 3) {
            return COUNTRIES[y];
        } else if (x > 3 && x < 7) {
            return names[z];
        } else {
            return COUNTRIES[y] + " " + names[z];
        }

    }

}
