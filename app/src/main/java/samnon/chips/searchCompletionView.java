package samnon.chips;

import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;
import android.view.CollapsibleActionView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.tokenautocomplete.TokenCompleteTextView;

public class searchCompletionView extends TokenCompleteTextView<filterDataClass> implements CollapsibleActionView {

    public searchCompletionView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected View getViewForObject(filterDataClass object) {
        LayoutInflater l = (LayoutInflater) getContext().getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        LinearLayout v = (LinearLayout) l.inflate(R.layout.search_token, (ViewGroup) getParent(), false);
        TextView view = v.findViewById(R.id.searchTerm);
        view.setText(object.getType() + " : " + object.getFilterString());
        ImageView imageView = v.findViewById(R.id.searchImage);
        imageView.setImageResource(object.getResouce());
        return v;
    }

    @Override
    protected filterDataClass defaultObject(String completionText) {
        return new filterDataClass("", -1, "keyword");
    }

    @Override
    public void onActionViewExpanded() {

    }

    @Override
    public void onActionViewCollapsed() {

    }
}
