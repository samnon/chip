package samnon.chips;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class dataAdapter extends BaseAdapter implements Filterable {

    private ArrayList<filterDataClass> dataClasses;
    private final dataFilter filter;


    public dataAdapter(ArrayList<filterDataClass> dataClasses) {

        this.dataClasses = dataClasses;
        filter = new dataFilter();
    }

    @Override
    public int getCount() {
        return dataClasses.size();
    }

    @Override
    public Object getItem(int position) {
        return dataClasses.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item, parent, false);
        }

        TextView textView = convertView.findViewById(R.id.textView);
        ImageView imageView = convertView.findViewById(R.id.imageView);

        if (getItem(position) instanceof filterDataClass) {
            filterDataClass item = (filterDataClass) getItem(position);
            textView.setText(item.getType() + "  :  " + item.getFilterString());
            imageView.setImageResource(item.getResouce());
        }
        return convertView;
    }

    @Override
    public Filter getFilter() {
        return filter;
    }


    public class dataFilter extends Filter {
        private Object lock = new Object();

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults results = new FilterResults();
            if (constraint != null && constraint.length() != 0) {
                final String searchStrLowerCase = constraint.toString().toLowerCase();

                ArrayList<filterDataClass> matchValues = new ArrayList<>();
                filterDataClass keyword = new filterDataClass(searchStrLowerCase, R.drawable.twotone_search_white_24, "Keyword");
                matchValues.add(keyword);

                if (searchStrLowerCase.contains("/")) {
                    filterDataClass date = new filterDataClass(searchStrLowerCase, R.drawable.twotone_calendar_today_white_24, "date");
                    matchValues.add(date);
                }
                if (searchStrLowerCase.matches("^[$0-9.]*$")) {
                    filterDataClass amount = new filterDataClass(searchStrLowerCase, R.drawable.twotone_attach_money_white_24, "amount");
                    matchValues.add(amount);
                }
                if (searchStrLowerCase.matches("^[0-9]*$") && searchStrLowerCase.length() ==4) {
                    filterDataClass last4Digits = new filterDataClass(searchStrLowerCase, R.drawable.twotone_credit_card_white_24, "last4Digits");
                    matchValues.add(last4Digits);
                }
                results.values = matchValues;
                results.count = matchValues.size();
            }
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            if (results.values != null) {
                dataClasses = (ArrayList<filterDataClass>) results.values;
            } else {
                dataClasses = new ArrayList<>();
            }
            if (results.count > 0) {
                notifyDataSetChanged();
            } else {
                notifyDataSetInvalidated();
            }
        }
    }
}


